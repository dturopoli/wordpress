<?php  


class Test_Custom_Post_Types{

    const PROJEKTI = "projekti";
	const CLANOVI_TIMA = "clanovi_tima";

	public function __constructor(){

	}

	public function init(){
		add_action('init', [$this, 'register_post_types']);
	}

	public function register_post_types(){
        $this->projekti();
		$this->clanovi_tima();
	}

	private function projekti(){
		$labels = [
			'name'               =>  'Projekti',
            'singular_name'      =>  'Projekt',
            'menu_name'          =>  'Projekti',
            'name_admin_bar'     =>  'Popis projekata',
            'add_new'            =>  'Dodaj novi projekt',
            'add_new_item'       =>  'Dodaj novi projekt',
            'new_item'           =>  'Novi projekt',
            'edit_item'          =>  'Uredi projekt',
            'view_item'          =>  'Pogledaj projekte',
            'all_items'          =>  'Svi projekti',
            'search_items'       =>  'Pretraži projekte',
            'parent_item_colon'  =>  'Roditelj projekta:',
            'not_found'          =>  'Nisu pronađeni projekti.',
            'not_found_in_trash' =>  'Nisu pronađeni projekti u smeću.',
            'has_archive'           => 'projekti',
		];

        register_post_type( self::PROJEKTI, [
                'labels' => $labels,
                'public' => true,
                'has_archive' => true,
                'supports' => [],
                'taxonomies' => [],
                'exclude_from_search' => false,
                'show_in_rest' => true,
                'menu_icon' => 'dashicons-admin-multisite',
                'publicly_queryable' => true,
            ]
        );
	}

    private function clanovi_tima(){
        $labels = [
            'name'               =>  'Članovi tima',
            'singular_name'      =>  'Član tima',
            'menu_name'          =>  'Članovi tima',
            'name_admin_bar'     =>  'Članovi tima',
            'add_new'            =>  'Dodaj člana',
            'add_new_item'       =>  'Dodaj člana',
            'new_item'           =>  'Novi član',
            'edit_item'          =>  'Uredi člana',
            'view_item'          =>  'Pogledaj člana',
            'all_items'          =>  'Svi članovi',
            'search_items'       =>  'Pretraži članovi',
            'parent_item_colon'  =>  'Roditelj članovi:',
            'not_found'          =>  'Nisu pronađeni članovi.',
            'not_found_in_trash' =>  'Nisu pronađeni članovi u smeću.'
        ];

        register_post_type( self::CLANOVI_TIMA, [
                'labels' => $labels,
                'public' => true,
                'has_archive' => true,
                'supports' => ['revision'],
                'taxonomies' => [],
                'exclude_from_search' => false,
                'show_in_rest' => true,
                'menu_icon' => 'dashicons-universal-access',
                'publicly_queryable' => true,
            ]
        );
    }
}
	