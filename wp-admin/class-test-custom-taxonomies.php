<?php  

class Test_Custom_Taxonomies{

    const PROJEKT_REFERENCE = "projekt_reference";

  
    function __constructor(){
    }

    function init(){
        add_action('init', [$this, 'register_taxonomies'] );
    }

    public function register_taxonomies(){
        $this->register_projekt_reference();

    }
    public function register_projekt_reference(){
        $labels = [
            'name'              => 'Reference',
            'singular_name'     => 'Reference',
            'search_items'      => 'Pretrazi reference',
            'all_items'         => 'Sve reference',
            'edit_item'         => 'Uredi referencu',
            'update_item'       => 'Spremi referencu',
            'add_new_item'      => 'Dodaj novu referencu',
            'new_item_name'     => 'Ime nove reference',
            'menu_name'         => 'Dodaj referencu',
        ];
        $args = [
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'capabilities' => array(
               
            ),


        ];
        register_taxonomy( self::PROJEKT_REFERENCE, [ Test_Custom_Post_Types::PROJEKTI ], $args );
    }
}