<?php  

class Projekt_Status_Metabox{

	function __construct() {
		add_action( 'add_meta_boxes', [$this, 'add_meta_boxes']);
	}

	public function add_meta_boxes() {

		add_meta_box(
			'project_status',
			'Status',
			[$this, 'output'],
			Test_Custom_Post_Types::PROJEKTI
		);
	}

	public function output() {
		?>
		<label for="project_status">Status</label>
		<select name="project_status" >
			<option value="izgradeno">Izgrađeno</option>
			<option value="neizgradeno">Neizgrađeno</option>
		</select>
	    <?php 
	}
}
