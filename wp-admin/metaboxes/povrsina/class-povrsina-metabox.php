<?php  

class Povrsina_Metabox{

	function __construct() {
		add_action( 'add_meta_boxes', [$this, 'add_meta_boxes']);
	}

	public function add_meta_boxes() {

		add_meta_box(
			'povrsina',
			'Povrsina',
			[$this, 'output'],
			Test_Custom_Post_Types::PROJEKTI
		);
	}

	public function output() {
		?>
		<label for="investitor">Povrsina</label>
		<input name="investitor" type="number"> m2
	    <?php 
	}
}