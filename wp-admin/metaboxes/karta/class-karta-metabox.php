<?php  

class Email_Metabox{

	function __construct() {
		add_action( 'add_meta_boxes', [$this, 'add_meta_boxes']);
	}

	public function add_meta_boxes() {

		add_meta_box(
			'email',
			'Email',
			[$this, 'output'],
			Test_Custom_Post_Types::PROJEKTI
		);
	}

	public function output() {
		?>
		<label for="email">Email</label>
		<input name="email" type="email"> 
	    <?php 
	}
}
