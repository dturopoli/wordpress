<?php  

class Vrsta_Nekretnine_Metabox{

	function __construct() {
		add_action( 'add_meta_boxes', [$this, 'add_meta_boxes']);
	}

	public function add_meta_boxes() {

		add_meta_box(
			'type_of_real_estate',
			'Vrsta Nekretnine',
			[$this, 'output'],
			Test_Custom_Post_Types::PROJEKTI
		);
	}

	public function output() {
		?>
		<label for="type_of_real_estate">Vrsta nekretnine</label>
		<select name="type_of_real_estate" >
			<option value="zemljiste">Zemljiste</option>
			<option value="apartman">Apartman</option>
			<option value="hotel">Hotel</option>
		</select>
	    <?php 
	}
}
