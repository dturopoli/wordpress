<?php  

class Cijena_Metabox{

	function __construct() {
		add_action( 'add_meta_boxes', [$this, 'add_meta_boxes']);
	}

	public function add_meta_boxes() {

		add_meta_box(
			'cijena',
			'Cijena',
			[$this, 'output'],
			Test_Custom_Post_Types::PROJEKTI
		);
	}

	public function output() {
		?>
		<label for="cijena">Cijena</label>
		<input name="cijena" type="number"> €
	    <?php 
	}
}