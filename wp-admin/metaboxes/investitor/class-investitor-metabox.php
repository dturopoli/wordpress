<?php  

class Investitor_Metabox{

	function __construct() {
		add_action( 'add_meta_boxes', [$this, 'add_meta_boxes']);
	}

	public function add_meta_boxes() {

		add_meta_box(
			'investitor',
			'Investitor',
			[$this, 'output'],
			Test_Custom_Post_Types::PROJEKTI
		);
	}

	public function output() {
		?>
		<label for="investitor">Investitor</label>
		<input name="investitor" type="text">
	    <?php 
	}
}