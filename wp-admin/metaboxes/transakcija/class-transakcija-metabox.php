<?php  

class Transakcija_Metabox{

	function __construct() {
		add_action( 'add_meta_boxes', [$this, 'add_meta_boxes']);
	}

	public function add_meta_boxes() {

		add_meta_box(
			'transackija',
			'Transakcija',
			[$this, 'output'],
			Test_Custom_Post_Types::PROJEKTI
		);
	}

	public function output() {
		?>
		<label for="transakacija">Transakcija</label>
		<select name="transakacija" id="">
			<option value="za_prodaju">za prodaju</option>
			<option value="za_najam">za najam</option>
		</select>
	    <?php 
	}
}