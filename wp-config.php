<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bBCiLf[/0 vH34xd~]v_EF`8dY%z80aaEGq,-^z&6v(HrcAYc+dlqU~3NYq3]xBT');
define('SECURE_AUTH_KEY',  '9B_as?zr)PCv?<;@Soge33$.f1*`.5b0JZ0YOL,/ut#$-@} .x=B>a+R&5U(1x[|');
define('LOGGED_IN_KEY',    'zy-QfjBrdldJDTuq8 s@N;|fsv<=s-b[MaLD`CKF8HRVfuviiQLE=Y4ok8P/]k+3');
define('NONCE_KEY',        'y9C$9>E;W&J,=KI:QfjjE?x:Q8rk-NIa[_wgcN@1./(Ts&5Z5[XVc>yQ^^tu`6^T');
define('AUTH_SALT',        'iszo86g$gITr&[hkM=9uXR!&MHE&I]O3U-_hZ1*$$#;=?h`hf71b074(CDk%mURS');
define('SECURE_AUTH_SALT', '=](}g@4?KPu%g+a)ql8bCZyHJ/=+pzfI1BuZ6c.U$MM3UB<3 %M[:!5!%OoPjqQL');
define('LOGGED_IN_SALT',   'e$Qn-Re{HslmHB$C3Gv34 =k/^dnW.gXzLPz:y:el@# zH_wdGg!rjcLe2k]K4qz');
define('NONCE_SALT',       'BD?HDV-U9Is$Va+*V48uytJlb-d|=R:76_n&YCw_|m0r*;t4Wrxl4`1LNYp&V2G+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
