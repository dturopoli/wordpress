<?php get_header(); ?>


    <!-- Page Content -->
    <div class="container">

  

		<?php 

			if (have_posts()) : while (have_posts()) : the_post() 

		?>

        <!-- Post Content Column -->
        <div class="row">

          <!-- Title -->
        <div class="col-lg-12">
          <h1 class="mt-4"><?php the_title(); ?></h1>
		</div>
		<div class="col-lg-12">
    

          	<div class="col-lg-12">
	          	<p class="lead">
	             <?php the_terms($post->id, 'projekt_reference') ?>
	          	</p>
			</div>
          

          <hr>
		
          <div class="col-lg-12">
          	<p><?php the_content(); ?></p>
          </div>

          <hr>

          <!-- Preview Image -->
          	<p>Godina: <?php the_field('godina'); ?></p>
			<p>Vrsta nekretnine: <?php 
				$field = get_field_object('vrsta_nekretnine');
				$value = $field['value'];
				$label = $field['choices'][$value];
				echo $label;
			?></p>
			<p>Status: <?php 
				$field = get_field_object('status');
				$value = $field['value'];
				$label = $field['choices'][$value];
				echo $label;
			?></p>
			<p>Investitor: <?php the_field('investitor'); ?></p>
			<p>Površina: <?php the_field('povrsina'); ?> m2</p>
			<p>Transakcija: <?php 
				$field = get_field_object('transakcija');
				$value = $field['value'];
				$label = $field['choices'][$value];
				echo $label;
			?></p>
			<p>Cijena: <?php the_field('cijena'); ?>€</p>

          <hr>

          <!-- Post Content -->
          <p class="lead"></p>

          </div>
          </div>


          <hr>

		<?php 

			endwhile;
		endif;

		 ?>

        </div>


     

    </div>
    <!-- /.container -->

<?php get_footer() ?>

