<?php get_header(); ?>


    <!-- Page Content -->
    <div class="container">

  

		<?php 

			if (have_posts()) : while (have_posts()) : the_post() 

		?>

        <!-- Post Content Column -->
        <div class="row">

          <!-- Title -->
        <div class="col-lg-12">
          <h1 class="mt-4"><?php the_title(); ?></h1>
		</div>
		<div class="col-lg-12">
    

          	<div class="col-lg-12">
	          	<p class="lead">
	             <?php the_terms($post->id, 'projekt_reference') ?>
	          	</p>
			</div>
          

          <hr>

          <hr>

          <!-- Preview Image -->
          	<p>Ime: <?php the_field('ime'); ?></p>
    			<p>Prezime: <?php the_field('prezime'); ?></p>
    			<p>Pozicija: <?php the_field('pozicija'); ?> </p>


          <hr>

          <!-- Post Content -->
          <p class="lead"></p>

          </div>
          </div>


          <hr>

		<?php 

			endwhile;
		endif;

		 ?>

        </div>


     

    </div>
    <!-- /.container -->

<?php get_footer() ?>

