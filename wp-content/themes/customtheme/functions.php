<?php  

include 'C:\xampp\htdocs\wordpress\wp-admin\class-test-custom-post-types.php';
include 'C:\xampp\htdocs\wordpress\wp-admin\class-test-custom-taxonomies.php';

/**
*	Register Custom Post Types
*/

$customPostTypes = new Test_Custom_Post_Types();
$customPostTypes->init();


/**
*	Register Custom Taxonomies
*/

$customTaxonomies = new Test_Custom_Taxonomies();
$customTaxonomies->init();


/**
*	Adding custom colums to projekti CPT
*/


	add_filter( 'manage_projekti_posts_columns', 'set_custom_projekti_columns' );

	function set_custom_projekti_columns($columns) {
	    unset( $columns['author'] );
	    unset( $columns['date'] );
	    $columns['cijena'] =  'Cijena';
	    $columns['povrsina'] = 'Povrsina';
	    $columns['status'] = 'Status';

	    return $columns;
	}

	add_action('manage_projekti_posts_custom_column', 'projekti_custom_columns', 10, 2);
	function projekti_custom_columns($column, $post_id) {
		switch ($column) {
			case 'cijena':
				echo the_field('cijena') . '$';
				break;
			case 'povrsina':
				echo the_field('povrsina') . 'm2';
				break;
			case 'status':
				echo the_field('status');
				break;
		}
	}

/**
*	Adding custom colums to clanovi_tima CPT
*/


	add_filter( 'manage_clanovi_tima_posts_columns', 'set_custom_clanovi_tima_columns' );

	function set_custom_clanovi_tima_columns($columns) {
	    unset( $columns['title'] );
	    unset( $columns['date'] );
	    $columns['ime'] =  'Ime i prezime';
	    $columns['pozicija'] = 'Pozicija';

	    return $columns;
	}

	add_action('manage_clanovi_tima_posts_custom_column', 'clanovi_tima_custom_columns', 10, 2);
	function clanovi_tima_custom_columns($column, $post_id) {
		switch ($column) {
			case 'ime':
				edit_post_link(get_field('ime') . " " . get_field('prezime'), '<a>', '</a>', $post_id);
				break;
			case 'pozicija':
				echo the_field('pozicija');
				break;
		}
	}

/**
*	Remove date filter from clanovi_tim CPT 
*/

function remove_date_drop(){

	$screen = get_current_screen();

	    if ( 'clanovi_tima' == $screen->post_type ){
	        add_filter('months_dropdown_results', '__return_empty_array');
	    }
}

add_action('admin_head', 'remove_date_drop');


/**
*	Add styles 
*/

function theme_styles() {

	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'main_css', get_template_directory_uri() . '/style.css' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles');

function theme_js() {

	global $wp_scripts;

	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/assets/css/bootstrap/js/bootstap.min.js');

}

add_action( 'wp_enqueue_scripts', 'theme_js');