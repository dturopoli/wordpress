<?php  


class Test_Custom_Post_Types{

	const PROJEKTI = "projekti";

	public function __constructor(){

	}

	public function init(){
		add_action('init', [$this, 'register_post_types']);
	}

	public function register_post_types(){
		$this->projekti();
	}

	private function projekti(){
		$labels = [
			'name'               =>  'Projekti',
            'singular_name'      =>  'Projekt',
            'menu_name'          =>  'Projekti',
            'name_admin_bar'     =>  'Popis projekata',
            'add_new'            =>  'Dodaj novi projekt',
            'add_new_item'       =>  'Dodaj novi projekt',
            'new_item'           =>  'Novi projekt',
            'edit_item'          =>  'Uredi projekt',
            'view_item'          =>  'Pogledaj projekte',
            'all_items'          =>  'Svi projekti',
            'search_items'       =>  'Pretraži projekte',
            'parent_item_colon'  =>  'Roditelj projekta:',
            'not_found'          =>  'Nisu pronađeni projekti.',
            'not_found_in_trash' =>  'Nisu pronađeni projekti u smeću.'
		];

        register_post_type( self::PROJEKTI, [
                'labels' => $labels,
                'public' => true,
                'has_archive' => true,
                'supports' => ['title', 'editor', 'thumbnail', 'excerpt'],
                'taxonomies' => [],
                'exclude_from_search' => false,
                'show_in_rest' => true,
                'menu_icon' => 'dashicons-admin-multisite',
                'publicly_queryable' => true,
            ]
        );
	}
}
	