<?php
/*
Plugin Name: Test Plugin
Description: This is a test plugin
Author: Dino

*/

if( ! defined( 'ABSPATH' ) ) exit;

function test_table() {
 global $wpdb;
 $charset_collate = $wpdb->get_charset_collate();
 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

 //* Create the teams table
 $table_name = $wpdb->prefix . 'test';
 $sql = "CREATE TABLE $table_name (
 id INTEGER NOT NULL AUTO_INCREMENT,
 test_name TEXT NOT NULL,

 PRIMARY KEY (id)
 ) $charset_collate;";
 dbDelta( $sql );
}
register_activation_hook( __FILE__, 'test_table' );